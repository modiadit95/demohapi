import {
  enterDetails,
  getDetails,
} from '../controllers/controller';

export const Router = {
  enter: {
    method: 'POST',
    path: '/api/enterDetails',
    config: {
      handler: enterDetails,
    },
  },
  get: {
    method: 'GET',
    path: '/api/getDetails',
    handler: getDetails,
  },
};
