import Hapi from 'hapi';
import { Router } from './routes/route';

const server = new Hapi.Server();
server.connection({
  port: 3001,
  host: 'localhost',
});

server.route(Router.enter);
server.route(Router.get);

server.start((err) => {
  if (err) {
    throw err;
  }
  console.log(`Connection established succesfully at : ${server.info.uri}`);
});
