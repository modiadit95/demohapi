import model from '../models/index';

export const enterDetails = (req, res) => {
  const item1 = model.Users.build({
    username: req.payload.username,
    uploadedBy: req.payload.uploadedBy,
    status: req.payload.status,
    rows: req.payload.rows,
    createdAt: new Date(),
    updatedAt: new Date(),
  });
  item1.save().then(() => {
    console.log('Items inserted succsefully');
    res('Items inserted successfully');
  }).catch((err) => {
    console.log(`Items not inserted: ${err}`);
    res(`Items not inserted: ${err}`);
  }).done();
};

export const getDetails = (req, res) => {
  model.Users.findAll({}).then((data) => {
    console.log(data);
    res(data);
  }).catch((err) => {
    console.log('Error in getting items' + err);
    res('Error in getting items' + err);
  }).done();
};
